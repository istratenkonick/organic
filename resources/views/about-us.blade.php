@extends('layouts.app')
    @section('content')

			<section class="breadcrumb-area" style="background-image:url({{ asset('images/background/2.jpg')}});">
			    <div class="container">
			        <div class="row">
			            <div class="col-md-12">
			                <div class="breadcrumbs text-center">
			                    <h1>{{ setting('about.breadcrumb_title') }}</h1>
			                    <h4>{{ setting('about.breadcrumb_subtitle') }}</h4>
			                </div>
			            </div>
			        </div>
			    </div>
				<div class="breadcrumb-bottom-area">
				    <div class="container">
				        <div class="row">
				            <div class="col-lg-8 col-md-5 col-sm-5">
				                <ul>
                                    <li><a href="{{ route('index') }}">Home</a></li>
                                    <li><i class="fa fa-angle-right"></i></li>
                                    <li>about-us</li>
				                </ul>
				            </div>
				            <div class="col-lg-4 col-md-7 col-sm-7">
				                <p>{{ setting('about.breadcrumb_description') }}</p>
				            </div>
				        </div>
				    </div>
				</div>
			</section>

			<section class="about-story">
				<div class="container">
					<div class="row">
						<div class="col-md-6 col-sm-12">
							<figure class="img-holder">
								<img src="{{ Voyager::image(setting('about.story_image')) }}" alt="">
							</figure>
						</div>
						<div class="col-md-6 col-sm-12">
							<div class="about-text">
								<div class="theme_title">
									<h3>{{ setting('about.story_title') }}</h3>
								</div>
								<div class="about_quot">
                                    {{ setting('about.story_subtitle') }}
                                </div>
								<div class="text"><p>{{ setting('about.story_description') }}</p></div>
								<div class="icon-box">
									<div class="single-item">
										<div class="icon"><i class="icon-wheat"></i></div>
										<div class="count">{{ setting('about.own_farm_houses') }}</div>
										<div class="name color1">{{ setting('about.own_farm_houses_title') }}</div>
									</div>
									<div class="single-item">
										<div class="icon"><i class="icon-nature-1"></i></div>
										<div class="count">{{ setting('about.pastoral_farmers') }}</div>
										<div class="name color1">{{ setting('about.pastoral_farmers_title') }}</div>
									</div>
								</div>
								<div class="text">
									<p>{{ setting('about.story_short_description') }}</p>
								</div>
								<div class="link">
									<a href="#" class="tran3s">{{ setting('about.story_button') }}</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>

			<section class="three-column">
				<div class="contaner-fluid">
					<div class="row">
                         @foreach($benefits as $item)
                                <div class="col-md-4 col-sm-12 col-xs-12 color1_bg text-center">
                                    <div class="single-item ">
                                        <h5>{{ $item->title }}</h5>
                                        <div class="icon"><i class="{{ $item->icon }}"></i></div>
                                        {!! $item->description !!}
                                        <div class="link"><a href="{{ route('products') }}">{{ setting('about.advantages_button') }}</a></div>
                                    </div>
                                </div>
                         @endforeach
					</div>
				</div>
			</section>
			<section class="four-column">
				<div class="container text-center">
					<div class="theme_title center">
						<h3>{{ setting('about.delivery_title') }}</h3>
					</div>
					<div class="row">
                        @foreach($deliveries as $item)
                            <div class="col-md-3 col-sm-6 col-xs-12 text-center">
                                <div class="single-item ">
                                    <div class="inner-box"><div class="icon"><i class="{{ $item->icon }}"></i><span>{{ $item->order }}</span></div></div>
                                    <h5>{{ $item->title }}</h5>
                                    {!! $item->description !!}
                                </div>
                            </div>
                        @endforeach
					</div>
					<div class="link"><a href="{{ route('products') }}" class="rot tran3s color1_bg">{{ setting('about.delivery_button') }}</a></div>
				</div>
			</section>

			<section class="gallery gallery-grid about-gallery" style="background-image:url({{ asset('images/background/3.jpg')}});">
			    <div class="tab-links">
			        <div class="container">
			        	<div class="iblock">
			        		<div class="theme_title text-left">
				                <h2>{{ setting('about.gallery_title') }}</h2>
				            </div>
			        	</div>

			            <ul class="tab-list">
                            @foreach($categories as $item)
			                <li>
                                <a href="#{{ $item->slug }}" class="tab-btn {{$loop->first ? 'active' : ''}}">
                                    <h2><i class="icon-food-2"></i>{{ $item->title }}</h2>
                                </a>
                            </li>
                            @endforeach
			            </ul>
			            <div class="link-btn"><a href="{{ route('products') }}" class="tran3s">{{ setting('about.gallery_button') }}<span class="fa fa-sort-desc"></span></a></div>
			        </div>
			    </div>

			                    <!--tab Content-->
			    <div class="tab-content">
			        <div class="container-fluid">
			            <!--tab Details / Collapsed-->

                        @foreach($categories as $category)
                            <div class="item {{$loop->first ? 'collapsed' : ''}}" id="{{ $category->slug }}">
                                <div class="row-10">
                                    <!--Default Item-->
                                    @foreach($category->products()->active()->get() as $product)
                                        <div class="col-md-2 column-2 col-sm-6 col-xs-12 default-item">
                                        <div class="inner-box">
                                            <div class="single-item center">
                                                <figure class="image-box">
                                                    <img src="{{ Voyager::image($product->img) }}" alt="">
                                                </figure>
                                                <div class="overlay-box">
                                                    <div class="inner">
                                                        <div class="image-view">
                                                            <div class="icon-holder">
                                                                <a href="{{  Voyager::image($product->img) }}" class="fancybox"><span class="icon-magnifier"></span></a>
                                                            </div>
                                                        </div>
                                                        <div class="bottom-content">
                                                            <h4>
                                                                <a href="{{ route('products') }}">{{ $product->title }}</a>
                                                            </h4>
                                                            <div class="price">${{ $product->price }}
                                                                <span class="prev-rate">${{ $product->old_price }}</span>
                                                            </div>
                                                            <div class="icon-box">
                                                                <a href="shop-cart.html">
                                                                    <span class="icon-icon-32846"></span>
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                        @endforeach
                    </div>
			    </div>
			</section>

			<section class="award">
				<div class="container">
					<div class="row">
						<div class="col-md-6 col-sm-12">
							<div class="award-info">
								<div class="theme_title">
									<h2>{{ setting('about.awards_recognition_title') }}</h2>
								</div>
							</div>
							<div class="text">
								<p>{{ setting('about.awards_recognition_description') }}</p>
							</div>
							<div class="award-logo">
								<ul>
                                    @foreach($awards as $item)
									<li><img src="{{ Voyager::image($item->img) }}" alt=""></li>
									@endforeach
								</ul>
							</div>
							<div class="customer-text">
								<p><b>{{ setting('about.support_title') }}</b> {{ setting('about.support_description') }}</p>
								<p><b>{{ setting('about.working_hours_title') }}</b> {{setting('about.working_hours')}}</p>
							</div>
						</div>
						<div class="col-md-6 col-sm-12">
							<div class="contact_in-box">
				                <div class="theme-title ">
				                    <h2>{{ setting('about.message_title') }}</h2>
				                </div>
				                <form action="{{ route('message') }}" method="POST">
                                    @csrf
				                	<div class="row">
					                    <div class="col-md-6">
					                        <input type="text" name="name" placeholder="Your Name*">
					                    </div>
					                    <!-- /.col-md-6 -->
					                    <div class="col-md-6">
					                        <input type="text" name="email" placeholder="Your Email*">
					                    </div>
					                    <!-- /.col-md-6 -->
					                    <div class="col-md-6">
					                        <input type="text" name="phone" placeholder="Phone">
					                    </div>
					                    <!-- /.col-md-6 -->
					                    <div class="col-md-6">
					                        <input type="text" name="subject" placeholder="Subject">
					                    </div>
					                    <!-- /.col-md-6 -->

					                    <div class="col-md-12">
					                        <textarea placeholder="Comments" name="comment"></textarea>
					                    </div>
					                    <!-- /.col-md-12 -->
					                    <div class="col-md-12">
					                        <button type="submit" class="color1_bg">Post Comment</button>
					                    </div>
                                        @if($errors->any())
                                            @foreach($errors->all() as $error)
                                                <p style="color: red; padding: 10px 15px; font-size: 30px">{{ $error }}</p>
                                            @endforeach
                                        @endif
                                        @if(session()->has('msg'))
                                            <div><p style="color: red; padding: 10px 15px; font-size: 30px">Success!!!</p></div>
                                    @endif
					                    <!-- /.col-md-12 -->
					                </div>
					                <!-- /.row -->
				                </form>
				            </div>
						</div>
					</div>
				</div>
			</section>

			@include('layouts.components._subscribe')
    @endsection
