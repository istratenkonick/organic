@extends('layouts.app')
    @section('content')

			<section class="breadcrumb-area" style="background-image:url({{ asset('images/background/2.jpg')}});">
			    <div class="container">
			        <div class="row">
			            <div class="col-md-12">
			                <div class="breadcrumbs text-center">
			                    <h1>{{ setting('news-detail.breadcrumb_title') }}</h1>
			                    <h4>{{ setting('news-detail.breadcrumb_subtitle') }}</h4>
			                </div>
			            </div>
			        </div>
			    </div>
				<div class="breadcrumb-bottom-area">
				    <div class="container">
				        <div class="row">
				            <div class="col-lg-8 col-md-5 col-sm-5">
				                <ul>
				                    <li><a href="{{ route('index') }}">Home</a></li>
				                    <li><i class="fa fa-angle-right"></i></li>
                                    <li><a href="{{ route('blogs') }}">Blogs</a></li>
                                    <li><i class="fa fa-angle-right"></i></li>
				                    <li>{{ $blog->title }}</li>
				                </ul>
				            </div>
				            <div class="col-lg-4 col-md-7 col-sm-7">
				                <p>{{ setting('news-detail.breadcrumb_description') }}</p>
				            </div>
				        </div>
				    </div>
				</div>
			</section>

			<section class="news single_news_page with_sidebar news_single">
				<div class="container">
					<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12">
							<div class="single_left_bar">
								<div class="blogList_single_post clear_fix wow fadeInUp" style="visibility: visible; animation-name: fadeInUp;">
		        					<div class="img_holder">
										<img src="{{ Voyager::image($blog->img) }}" alt="News" class="img-responsive">
									</div> <!-- End of .img_holder -->
									<div class="post">
										<div class="text">
											<h4><a>{{ $blog->title }}</a></h4>
											<ul>
												<li><a class="tran3s"><i class="fa fa-tag" aria-hidden="true"></i>{{ $blog->category()->first()->title }}</a></li>
												<li><a class="tran3s"><i class="fa fa-clock-o" aria-hidden="true"></i>{{ $blog->created_at }}</a></li>
												<li><a class="tran3s"><i class="fa fa-comments" aria-hidden="true"></i>{{ $blog->views }}</a></li>
											</ul>
											<p>
                                                {{ $blog->short_description }}
                                            </p>
										</div>
											{!! $blog->description !!}
									</div> <!-- End of .post -->

									<div class="author-post">
										<div class="comment-box">
							                <div class="theme_title ">
							                    <h2>about author</h2>
							                </div>
							                <div class="single-comment-box">
							                    <div class="img-box">
							                        <img src="{{ Voyager::image($blog->user()->first()->avatar)}}" alt="Awesome Image">
							                    </div>
							                    <div class="text-box">
							                        <div class="top-box">
							                            <h2>{{ $blog->user()->first()->name }}</h2>
							                        </div>
							                        <div class="content">
							                            <p>{{ $blog->user()->first()->about }}</p>
							                            <ul class="social_icon author_icon">
									        				<li><a href="{{ setting('menu.facebook')}}" class="tran3s" title="Facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
									        				<li><a href="{{ setting('menu.twitter') }}" class="tran3s" title="Twitter"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
									        				<li><a href="{{ setting('menu.google') }}" class="tran3s" title="Linkedin"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
									        				<li><a href="{{ setting('menu.pinterest') }}" class="tran3s" title="Youtube"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
									        			</ul>
							                        </div>
							                    </div>
							                </div>
							            </div>
									</div>

									<div class="comment-box">
						                <div class="theme_title ">
						                    <h2>{{ setting('news-detail.comment_title') }} ({{ $blog->comments()->count() }})</h2>
						                </div>
                                        @foreach($blog->comments()->get() as $item)
						                <div class="single-comment-box">
						                    <div class="text-box">
						                    	<div class="clearfix">
						                    		 <div class="top-box float_left">
							                            <h2>{{ $item->name }}</h2>
							                        </div>
							                        <div class="float_right">
									      				<span class="p_color">{{ $item->created_at }} </span>
									      			</div>
						                    	</div>
						                        <div class="content">
						                            <p>{{ $item->comment }}</p>
						                        </div>
						                    </div>
						                </div>
                                        @endforeach
						            </div>
						            <div class="reply-box">
						                <div class="theme_title ">
						                    <h2>{{ setting('news-detail.comment_subtitle') }}</h2>
						                </div>
                                        <form action="{{ route('blog_comment', ['blog_id' => $blog->id]) }}" method="POST">
                                            @csrf
						                <div class="row">
						                    <div class="col-md-12">
						                        <textarea placeholder="Comments" name="comment"></textarea>
						                    </div>
						                    <div class="col-md-6">
						                        <input type="text" placeholder="Your Name*" name="name">
						                    </div>
						                    <div class="col-md-6">
						                        <input type="text" placeholder="Your Email*" name="email">
						                    </div>
						                    <div class="col-md-12">
						                        <input type="text" placeholder="Your Website" name="website">
						                    </div>
						                    <div class="col-md-12">
						                        <button type="submit" class="color1_bg">Post Comment</button>
						                    </div>
                                            @if($errors->any())
                                                @foreach($errors->all() as $error)
                                                    <p style="color: red; padding: 10px 15px; font-size: 30px">{{ $error }}</p>
                                                @endforeach
                                            @endif
                                            @if(session()->has('msg'))
                                                <div><p style="color: red; padding: 10px 15px; font-size: 30px">Success!!!</p></div>
                                        @endif
						                    <!-- /.col-md-12 -->
						                </div>
                                        </form>
						                <!-- /.row -->
						            </div>
		        				</div>
							</div>
						</div>
					</div>
				</div>
			</section>
    @endsection
