@extends('layouts.app')
@section('content')
        <section class="breadcrumb-area" style="background-image:url({{ asset('images/background/2.jpg')}});">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="breadcrumbs text-center">
                            <h1>{{ setting('store.breadcrumb_title') }}</h1>
                            <h4>{{ setting('store.breadcrumb_subtitle') }}</h4>
                        </div>
                    </div>
                </div>
            </div>
            <div class="breadcrumb-bottom-area">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-8 col-md-5 col-sm-5">
                            <ul>
                                <li><a href="{{ route('index') }}">Home</a></li>
                                <li><i class="fa fa-angle-right"></i></li>
                                <li>our store</li>
                            </ul>
                        </div>
                        <div class="col-lg-4 col-md-7 col-sm-7">
                            <p>{{ setting('store.breadcrumb_description') }}</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!-- Shop Page Content************************ -->
        <div class="shop_page featured-product">
            <div class="container">
                <div class="row">
                    <div class="col-lg-9 col-md-8 col-sm-12 col-sx-12">
                        <div class="row">
                            @foreach($products as $product)
                                @include('layouts.components._product-card')
                                @if($loop->iteration %4 === 0)
                                    <div class="clearfix"></div>
                                @endif
                            @endforeach
                        </div>
                    </div>

                    <!-- _______________________ SIDEBAR ____________________ -->
                    <div class="col-lg-3 col-md-4 col-sm-12 col-xs-12 sidebar_styleTwo">
                        <div class="wrapper">
                            <div class="sidebar_search">
                                <form action="#">
                                    <input type="text" name="s" value="{{ request()->get('s') }}">
                                    @if(request()->has('category_id'))
                                        <input type="hidden" name="category_id" value="{{ request()->get('category_id') }}">
                                    @endif
                                    @if(request()->has('price_min'))
                                        <input type="hidden" name="price_min" value="{{ request()->get('price_min') }}">
                                    @endif
                                    @if(request()->has('price_max'))
                                        <input type="hidden" name="price_max" value="{{ request()->get('price_max') }}">
                                    @endif
                                    <button class="tran3s color1_bg"><i class="fa fa-search" aria-hidden="true"></i></button>
                                </form>
                            </div> <!-- End of .sidebar_styleOne -->

                            <div class="sidebar_categories">
                                <div class="theme_inner_title">
                                    <h4>{{ setting('store.categories_title') }}</h4>
                                </div>
                                <ul>
                                    @foreach($categories as $item)
                                        @php
                                            $array = ['category_id' => $item->id];

                                            if (request()->has('s')) {
                                                $array['s'] = request()->get('s');
                                            }

                                            if (request()->has('price_min')) {
                                                $array['price_min'] = request()->get('price_min');
                                            }

                                            if (request()->has('price_max')) {
                                                $array['price_max'] = request()->get('price_max');
                                            }
                                        @endphp
                                        <li>
                                            <a href="{{ route('products', $array) }}" class="tran3s">{{ $item->title }}   ({{ $item->products_count }})</a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div> <!-- End of .sidebar_categories -->

                            <div class="price_filter wow fadeInUp">
                                <div class="theme_inner_title">
                                    <h4>{{ setting('store.filter_title') }}</h4>
                                </div>
                                <div class="single-sidebar price-ranger">
                                    <div id="slider-range"></div>
                                    <div class="ranger-min-max-block">
                                        <form action="">
                                            @if(request()->has('category_id'))
                                                <input type="hidden" name="category_id" value="{{request()->get('category_id')}}">
                                            @endif
                                            @if(request()->has('s'))
                                                <input type="hidden" name="s" value="{{request()->get('s')}}">
                                            @endif
                                            <input type="submit" value="Filter">
                                            <span>Price:</span>
                                            <input type="text" readonly class="min">
                                            <span>-</span>
                                            <input type="text" readonly class="max">
                                            <input type="hidden" value="{{ request()->get('price_min') }}" name="price_min" readonly class="min-hidden">
                                            <input type="hidden" value="{{ request()->get('price_max') }}" name="price_max" readonly class="max-hidden">
                                        </form>
                                    </div>
                                </div> <!-- /price-ranger -->
                            </div> <!-- /price_filter -->

                            <div class="best_sellers clear_fix wow fadeInUp">
                                <div class="theme_inner_title">
                                    <h4>{{ setting('store.products_title') }}</h4>
                                </div>
                                @foreach($popular as $item)
                                <div class="best_selling_item clear_fix border">
                                    <div class="img_holder float_left">
                                        <img src="{{ Voyager::image($item->img) }}" alt="image">
                                    </div> <!-- End of .img_holder -->
                                            <div class="text float_left">
                                                <a href="{{ route('product_detail', $item->slug)}}">
                                                    <h6>{{ $item->title }}</h6>
                                                </a>
                                                <ul>
                                                    @for($i = 0; $i < $item->star; $i++)
                                                     <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                                    @endfor
                                                </ul>
                                                <span>${{ $item->price }}</span>
                                            </div> <!-- End of .text -->
                                        </div> <!-- End of .best_selling_item -->
                                @endforeach
                            </div> <!-- End of .best_sellers -->
                        </div> <!-- End of .wrapper -->
                    </div> <!-- End of .sidebar_styleTwo -->
                </div> <!-- End of .row -->
            </div> <!-- End of .container -->
        </div> <!-- End of .shop_page -->
@endsection

@section('scripts')
    <script>
        if ($('.price-ranger').length) {
            $('.price-ranger #slider-range').slider({
                range: true,
                min: 0,
                max: 200,
                values: [{{ request()->get('price_min') ?? 0 }}, {{ request()->get('price_max') ?? 999999999 }}],
                slide: function(event, ui) {
                    $('.price-ranger .ranger-min-max-block .min').val('$' + ui.values[0]);
                    $('.price-ranger .ranger-min-max-block .max').val('$' + ui.values[1]);

                    $('.price-ranger .ranger-min-max-block .min-hidden').val(ui.values[0]);
                    $('.price-ranger .ranger-min-max-block .max-hidden').val(ui.values[1]);
                }
            });
            $('.price-ranger .ranger-min-max-block .min').val('$' + $('.price-ranger #slider-range').slider('values', 0));
            $('.price-ranger .ranger-min-max-block .max').val('$' + $('.price-ranger #slider-range').slider('values', 1));
        };
    </script>
@endsection

