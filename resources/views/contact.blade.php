@extends('layouts.app')
    @section('content')
			<section class="breadcrumb-area" style="background-image:url({{ asset('images/background/2.jpg')}});">
			    <div class="container">
			        <div class="row">
			            <div class="col-md-12">
			                <div class="breadcrumbs text-center">
			                    <h1>{{ setting('contact.breadcrumb_title') }}</h1>
			                    <h4>{{ setting('contact.breadcrumb_subtitle') }}</h4>
			                </div>
			            </div>
			        </div>
			    </div>
				<div class="breadcrumb-bottom-area">
				    <div class="container">
				        <div class="row">
				            <div class="col-lg-8 col-md-5 col-sm-5">
				                <ul>
				                    <li><a href="{{ route('index') }}">Home</a></li>
				                    <li><i class="fa fa-angle-right"></i></li>
				                    <li>Contact us</li>
				                </ul>
				            </div>
				            <div class="col-lg-4 col-md-7 col-sm-7">
				                <p>{{ setting('contact.breadcrumb_description') }}</p>
				            </div>
				        </div>
				    </div>
				</div>
			</section>

			<section class="single-contact_us">
				<div class="container">
					<div class="row">
						<div class="col-md-4 col-sm-6 col-xs-12">
							<div class="left_contact">
								<h5>{{ setting('footer.contact_title') }}</h5>
								<ul class="list catagories">
		                            <li><a href="{{ setting('footer.address') }}"><i class="fa fa-home color1"></i>{{ setting('footer.address') }}</a></li>
		                            <li><a href="mail:{{ setting('header.email') }} "><i class="fa fa-envelope color1"></i><span>{{ setting('header.email') }}</span></a></li>
		                            <li><a href="tel:{{ setting('header.phone') }}"><i class="fa fa-phone color1"></i>{{ setting('header.phone') }}</a></li>
		                        </ul>

		                        <div class="border-area">
			                        <h6>{{ setting('footer.business_hours_title') }}</h6>
									<div class="list Business">
			                            <p>{{ setting('footer.business_hours') }}</p>
			                        </div>
		                        </div>
							</div>
						</div>

						<div class="col-md-8 col-sm-6 col-xs-12">
			                <div class="contact_in-box">
				                <div class="theme-title ">
				                    <h2>{{ setting('contact.form_title') }}</h2>
				                </div>
				                <form action="{{ route('message') }}" method="POST">
                                    @csrf
				                	<div class="row">
					                    <div class="col-md-6">
					                        <input type="text" placeholder="Your Name*" name="name">
					                    </div>
					                    <div class="col-md-6">
					                        <input type="text" placeholder="Your Email*" name="email">
					                    </div>
					                    <div class="col-md-6">
					                        <input type="text" placeholder="Phone" name="phone">
					                    </div>
					                    <div class="col-md-6">
					                        <input type="text" placeholder="Subject" name="subject">
					                    </div>
					                    <div class="col-md-12">
					                        <textarea placeholder="Comments" name="comment"></textarea>
					                    </div>
					                    <div class="col-md-12">
					                        <button type="submit" class="color1_bg">{{ setting('contact.button') }}</button>
					                    </div>
                                        @if($errors->any())
                                            @foreach($errors->all() as $error)
                                                <p style="color: red; padding: 10px 15px; font-size: 30px">{{ $error }}</p>
                                            @endforeach
                                        @endif
                                        @if(session()->has('msg'))
                                            <div><p style="color: red; padding: 10px 15px; font-size: 30px">Success!!!</p></div>
                                    @endif
					                </div>
				                </form>
				            </div>
			            </div>
					</div>
				</div>
			</section>

			<!-- Google map************************ -->
			<section id="google-map-area">
                {!! setting('contact.map')   !!}
	   		</section>
    @endsection

