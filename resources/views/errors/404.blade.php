@extends('layouts.app')
@section('content')

    <div class="has-base-color-overlay sec-pad text-center error-404" style="background-image:url({{ asset('images/background/404.jpg') }})">
        <div class="container">
            <div class="col-md-10 col-md-offset-1">
                <div class="top-box">
                    <figure class="text-center"><img src="{{ asset('images/logo/404.png') }}" alt=""></figure>
                    <h2>Sorry but we couldnt find this page.</h2>
                </div><!-- /.top-box -->
                <p>The page you are looking for does not exist. <span>Back to Homepage!</span></p>
            </div><!-- /.col-md-10 col-md-offset-1 -->
        </div><!-- /.container -->
    </div>

@endsection

