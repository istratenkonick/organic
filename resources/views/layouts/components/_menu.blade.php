<div class="theme_menu color1_bg">
    <div class="container">
        <nav class="menuzord pull-left" id="main_menu">
            <ul class="menuzord-menu">
                <li @if(Route::currentRouteName() == 'index') class="current_page" @endif><a href="{{ route('index') }}">Home</a></li>
                <li @if(Route::currentRouteName() == 'about_us') class="current_page" @endif><a href="{{ route('about_us') }}">About us</a></li>
                <li @if(Route::currentRouteName() == 'products') class="current_page" @endif><a href="{{ route('products') }}">store</a></li>
                <li @if(Route::currentRouteName() == 'blogs') class="current_page" @endif><a href="{{ route('blogs') }}">News</a></li>
                <li @if(Route::currentRouteName() == 'faq') class="current_page" @endif><a href="{{ route('faq') }}">FAQ</a></li>
                <li @if(Route::currentRouteName() == 'contact_us') class="current_page" @endif><a href="{{ route('contact_us') }}">Contact us</a></li>
            </ul> <!-- End of .menuzord-menu -->
        </nav> <!-- End of #main_menu -->

        <!-- ******* Cart And Search Option ******** -->
        <div class="nav_side_content pull-right">
            <ul class="icon_header">
                <li class="border_round tran3s"><a href="{{ setting('menu.facebook')}}"><i class="fa fa-facebook"></i></a></li>
                <li class="border_round tran3s"><a href="{{ setting('menu.twitter') }}"><i class="fa fa-twitter"></i></a></li>
                <li class="border_round tran3s"><a href="{{ setting('menu.google') }}"><i class="fa fa-google-plus"></i></a></li>
                <li class="border_round tran3s"><a href="{{ setting('menu.pinterest') }}"><i class="fa fa-pinterest"></i></a></li>
            </ul>
        </div> <!-- End of .nav_side_content -->
    </div> <!-- End of .conatiner -->
</div> <!-- End of .theme_menu -->
