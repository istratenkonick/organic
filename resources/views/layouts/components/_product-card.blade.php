<div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 mix mix_all default-item all {{ $product->category->slug }}" style="display: inline-block;">
    <div class="inner-box">
        <div class="single-item center">
            <figure class="image-box"><img src="{{ Voyager::image($product->img) }}" alt="">
                @if($product->is_new)<div class="product-model new">New</div>@endif
                @if($product->is_hot)<div class="product-model hot">Hot</div>@endif
            </figure>
            <div class="content">
                <h3><a href="#">{{ $product->title }}</a></h3>
                <div class="rating">
                        @for($i = 0; $i < $product->star; $i++)
                                <span class="fa fa-star"></span>
                        @endfor
                </div>
                <div class="price">${{ $product->price }}<span class="prev-rate">${{ $product->old_price }}</span></div>
            </div>
            <div class="overlay-box">
                <div class="inner">
                    <div class="top-content">
                        <ul>
                            <form  method="POST" id="payment-form"  action="{{ route('paypal', ['id' => $product->id]) }}">
                                @csrf

                                <li class="tultip-op">
                                    <input style="display:none;">
                                   <i class="fa fa-sort-desc"></i><button>PayPal</button>
                                </li>

                            </form>
                        </ul>
                    </div>
                    <div class="bottom-content">
                        <h4><a href="{{ route('product_detail', $product->slug) }}">It Contains:</a></h4>
                        <p>{{ $product->short_description }}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

