@if($blogs->isNotEmpty() || $products->isNotEmpty())
<div id="search-results">
    <div class="search-wrapper">
        @if($products->isNotEmpty())
            <div class="title">Products:</div>
            @foreach($products as $item)
                <div class="search-card">
                    <a href="{{ route('product_detail', $item->slug) }}"><img src="{{ Voyager::image($item->img) }}" alt="">{{ $item->title }}</a>
                </div>
            @endforeach
        @endif
    </div>
    <div class="search-wrapper">
        @if($blogs->isNotEmpty())
            <div class="title">Blogs:</div>
            @foreach($blogs as $item)
                <div class="search-card">

                    <a href="{{ route('blog_detail', $item->slug) }}"><img src="{{ Voyager::image($item->img) }}" alt="">{{ $item->title }}</a>
                </div>
            @endforeach
        @endif
    </div>
</div>
@endif
