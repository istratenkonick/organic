<header>
    <div class="top_header">
        <div class="container">
            <div class="pull-left header_left">
                <ul>
                    <li><a href="tel:{{ setting('header.phone') }}">Order On Phone: <span>{{ setting('header.phone') }}</span></a></li>
                    <li><i class="fa fa-envelope-o s_color" aria-hidden="true"></i><a href="mailto:{{ setting('header.email') }}">{{ setting('header.email') }}</a></li>
                </ul>
            </div>
        </div> <!-- End of .container -->
    </div> <!-- End of .top_header -->

    <div class="bottom_header">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-sm-12 col-xs-12">
                    <div class="search-box" id="search-box">
                        <form action="#" class="clearfix">
                            <input type="text" placeholder="Search..." class="search" id="search" name="search" autocomplete="off">
                            <button><i class="fa fa-search"></i></button>
                        </form>
                    </div>
                </div>
                <div class="col-md-4 col-sm-5 col-xs-6 logo-responsive">
                    <div class="logo-area">
                        <a href="{{ route('index') }}" class="pull-left logo"><img src="{{ Voyager::image(setting('header.logo')) }}" alt="LOGO"></a>
                    </div>
                </div>
                <div class="col-md-4 col-sm-7 col-xs-6 pdt-14">
                </div>
            </div>
        </div>
    </div> <!-- End of .bottom_header -->
</header>
