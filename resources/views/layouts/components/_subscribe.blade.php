<section class="call-out">
    <div class="container">
        <div class="float_left">
            <h2>{{ setting('index.subscribe_title') }}</h2>
            <p>{{ setting('index.subscribe_subtitle') }}</p>
        </div>
        <div class="float_right">
            <div class="contact-box">
                <form method="POST" action="{{ route('subscribe') }}" class="contact-form">
                    @csrf
                    <div class="row clearfix">
                        <div class="form-group">
                            <input type="text" name="name"  placeholder="Your Name*"><i class="fa fa-user" aria-hidden="true"></i>
                        </div>
                        <div class="form-group">
                            <input type="email" name="email"  placeholder="Email Address*"><i class="fa fa-envelope" aria-hidden="true"></i>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn-style-one center">Submit now</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
