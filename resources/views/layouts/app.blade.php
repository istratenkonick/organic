<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from st.ourhtmldemo.com/template/organic_store/shop.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 06 Jul 2020 10:08:51 GMT -->
<head>
    <meta charset="UTF-8">
    <!-- For IE -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- For Resposive Device -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
{{--    <title>{{ $meta_title ?? '' }}</title>--}}
{{--    <title>{{ $meta_description ?? '' }}</title>--}}
{{--    {!! $og->renderTags() ?? '' !!}--}}
{{--    {!!  $graph ?? '' !!}--}}
    <meta name="_token" content="{{ csrf_token() }}">
    <!-- Favicon -->
    <link rel="apple-touch-icon" sizes="57x57" href="{{ asset('images/fav-icon/apple-icon-57x57.png')}}">
    <link rel="apple-touch-icon" sizes="60x60" href="{{ asset('images/fav-icon/apple-icon-60x60.png')}}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{ asset('images/fav-icon/apple-icon-72x72.png')}}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('images/fav-icon/apple-icon-76x76.png')}}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{ asset('images/fav-icon/apple-icon-114x114.png')}}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{ asset('images/fav-icon/apple-icon-120x120.png')}}">
    <link rel="apple-touch-icon" sizes="144x144" href="{{ asset('images/fav-icon/apple-icon-144x144.png')}}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{ asset('images/fav-icon/apple-icon-152x152.png')}}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('images/fav-icon/apple-icon-180x180.png')}}">
    <link rel="icon" type="image/png" sizes="192x192"  href="{{ asset('images/fav-icon/android-icon-192x192.png')}}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('images/fav-icon/favicon-32x32.png')}}">
    <link rel="icon" type="image/png" sizes="96x96" href="{{ asset('images/fav-icon/favicon-96x96.png')}}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('images/fav-icon/favicon-16x16.png')}}">
    <link rel="stylesheet" href="{{ asset('css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">

    <!-- Custom Css -->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/style.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/responsive.css') }}">


    <!-- Fixing Internet Explorer ______________________________________-->

    <!--[if lt IE 9]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <script src="{{ asset('vendor/html5shiv.js')}}"></script>
    <![endif]-->
</head>

<body>
<div class="main_page">

    <!-- Header *******************************  -->
@include('layouts.components._header')

<!-- Menu *******************************  -->
@include('layouts.components._menu')
@yield('content')

<!-- Footer************************* -->
<x-footer></x-footer>

<!-- Scroll Top Button -->
<button class="scroll-top tran3s color2_bg">
    <span class="fa fa-angle-up"></span>
</button>
<!-- pre loader  -->
<div id="loader-wrapper">
    <div id="loader"></div>
</div>




<!-- Js File_________________________________ -->
{{--    Search box--}}

<!-- j Query -->
<script type="text/javascript" src="{{ asset('js/jquery-2.1.4.js')}}"></script>
<!-- Bootstrap JS -->
<script type="text/javascript" src="{{ asset('js/bootstrap.min.js')}}"></script>

<!-- Vendor js _________ -->
<!-- Google map js -->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCRvBPo3-t31YFk588DpMYS6EqKf-oGBSI"></script> <!-- Gmap Helper -->
<script src="{{ asset('js/gmap.js')}}"></script>
<!-- owl.carousel -->
<script type="text/javascript" src="{{ asset('js/owl.carousel.min.js')}}"></script>
<!-- ui js -->
<script type="text/javascript" src="{{ asset('js/jquery-ui.min.js')}}"></script>
<!-- Responsive menu-->
<script type="text/javascript" src="{{ asset('js/menuzord.js')}}"></script>
<!-- revolution -->
<script src="{{ asset('vendor/revolution/jquery.themepunch.tools.min.js')}}"></script>
<script src="{{ asset('vendor/revolution/jquery.themepunch.revolution.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('vendor/revolution/revolution.extension.slideanims.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('vendor/revolution/revolution.extension.layeranimation.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('vendor/revolution/revolution.extension.navigation.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('vendor/revolution/revolution.extension.kenburn.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('vendor/revolution/revolution.extension.actions.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('vendor/revolution/revolution.extension.parallax.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('vendor/revolution/revolution.extension.migration.min.js') }}"></script>

<!-- landguage switcher js -->
<script type="text/javascript" src="{{ asset('js/jquery.polyglot.language.switcher.js')}}"></script>
<!-- Fancybox js -->
<script type="text/javascript" src="{{ asset('js/jquery.fancybox.pack.js')}}"></script>
<!-- js count to -->
<script type="text/javascript" src="{{ asset('js/jquery.appear.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/jquery.countTo.js')}}"></script>
<!-- WOW js -->
<script type="text/javascript" src="{{ asset('js/wow.min.js')}}"></script>

<script type="text/javascript" src="{{ asset('js/SmoothScroll.js')}}"></script>

<script src="{{ asset('js/bootstrap-select.min.js')}}"></script>
<script src="{{ asset('js/jquery.mixitup.min.js')}}"></script>
<!-- Theme js -->
<script type="text/javascript" src="{{ asset('js/theme.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/google-map.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.21.1/axios.min.js" integrity="sha512-bZS47S7sPOxkjU/4Bt0zrhEtWx0y0CRkhEp8IckzK+ltifIIE9EMIMTuT/mEzoIMewUINruDBIR/jJnbguonqQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script>
    const search_input = document.querySelector('#search');
    search_input.addEventListener('keyup', function (evt) {
        const value = evt.target.value;

        if (value === '' || value.length < 3) {
            let table = document.querySelector('#search-results');

            if (table) {
                table.remove();
            }

            return;
        }

        axios.get('{{ route('search') }}?search=' + value).then(function (response) {
            let table = document.querySelector('#search-results');

            if (table) {
                table.remove();
            }

            const results = response.data;
            const search_box = document.querySelector('#search-box');
            search_box.insertAdjacentHTML('beforeend', results);
        })
    });

    document.querySelector('body').addEventListener('click', function (evt) {
        const wrapper = document.querySelector('#search-results');

        if (!wrapper) {
            return true;
        }

        const nodes = Array.from(wrapper.querySelectorAll("*"));

        if (evt.target === wrapper || nodes.includes(evt.target)) {
            //
        } else {
            let table = document.querySelector('#search-results');

            if (table) {
                table.remove();
            }
        }
    })
</script>
@yield('scripts')

</div> <!-- End of .main_page -->
</body>

<!-- Mirrored from st.ourhtmldemo.com/template/organic_store/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 06 Jul 2020 10:07:28 GMT -->
</html>
