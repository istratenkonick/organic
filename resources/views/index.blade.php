@extends('layouts.app')
@section('content')
			<!-- Banner ____________________________________ -->
	        <div id="banner">
	        	<div class="rev_slider_wrapper">
					<!-- START REVOLUTION SLIDER 5.0 auto mode -->
						<div id="main_slider" class="rev_slider" data-version="5.0">
							<ul>
                                @foreach($banners as $item)
                                    @if($item->type == 1)
                                        <li data-index='rs-377' data-transition='curtain-1' data-slotamount='1' data-easein='default' data-easeout='default' data-masterspeed='default' data-thumb='images/home/slide-1.jpg' data-rotate='0' data-saveperformance='off' data-title='Business Solutions' data-description='' >
									<!-- MAIN IMAGE -->
									<img src="{{ Voyager::image($item->img_background) }}"  alt="image"  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg">
									<!-- LAYERS -->
									<!-- LAYER NR. 1 -->
									<div class="tp-caption tp-resizeme rs-parallaxlevel-3"
										data-x="['center','center','center','center']" data-hoffset="['0','0','35','0']"
										data-y="['middle','middle','middle','middle']" data-voffset="['-120','-120','-120','-120']"
										data-width="none"
										data-height="none"
										data-transform_idle="o:1;"
										data-transform_in="z:0;rX:0deg;rY:0;rZ:0;sX:1.5;sY:1.5;skX:0;skY:0;opacity:0;s:1500;e:Power3.easeInOut;"
										data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;"
										data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
										data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
										data-start="1000"
										data-splitout="none"
										data-responsive_offset="on"
										data-elementdelay="0.05"
										style="z-index: 5;">
										<img src="{{ Voyager::image($item->img_icon) }}" alt="">
									</div>

									<!-- LAYER NR. 2 -->
									<div class="tp-caption tp-resizeme rs-parallaxlevel-3"
										data-x="['center','center','center','center']" data-hoffset="['0','0','35','0']"
										data-y="['middle','middle','middle','middle']" data-voffset="['50','50','50','50']"
										data-width="none"
										data-height="none"
										data-transform_idle="o:1;"
										data-transform_in="z:0;rX:0deg;rY:0;rZ:0;sX:1.5;sY:1.5;skX:0;skY:0;opacity:0;s:1500;e:Power3.easeInOut;"
										data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;"
										data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
										data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
										data-start="1500"
										data-splitout="none"
										data-responsive_offset="on"
										data-elementdelay="0.05"
										style="z-index: 5;">
										<h1>Organic Store</h1>
									</div>

									<!-- LAYER NR. 3 -->
									<div class="tp-caption tp-resizeme rs-parallaxlevel-2"
										data-x="['center','center','center','center']" data-hoffset="['0','0','35','0']"
										data-y="['middle','middle','middle','middle']" data-voffset="['110','110','110','110']"
										data-width="none"
										data-height="none"
										data-whitespace="nowrap"
										data-transform_idle="o:1;"

										data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;"
										data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;"
										data-mask_in="x:0px;y:[100%];"
										data-mask_out="x:inherit;y:inherit;"
										data-start="2000"
										data-splitin="none"
										data-splitout="none"
										data-responsive_offset="on"
										style="z-index: 6; white-space: nowrap;">
										<h5 class="cp-title">{{ $item->title }}</h5>
									</div>

									<!-- LAYER NR. 4 -->
									<div class="tp-caption tp-resizeme rs-parallaxlevel-2"
										data-x="['center','center','center','center']" data-hoffset="['0','0','35','0']"
										data-y="['middle','middle','middle','middle']" data-voffset="['160','160','160','160']"
										data-width="none"
										data-height="none"
										data-whitespace="nowrap"
										data-transform_idle="o:1;"

										data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;"
										data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;"
										data-mask_in="x:0px;y:[100%];"
										data-mask_out="x:inherit;y:inherit;"
										data-start="2500"
										data-splitin="none"
										data-splitout="none"
										data-responsive_offset="on"
										style="z-index: 6; white-space: nowrap;">
										<h5 class="cp-title-2">{{ $item->sub_title }}</h5>
									</div>
								</li>
                                    @elseif($item->type == 2)
                                            <li data-index="rs-18" data-transition="zoomin" data-slotamount="7"  data-easein="Power4.easeInOut" data-easeout="Power4.easeInOut" data-masterspeed="2000"  data-thumb="images/home/slide-2.jpg"  data-rotate="0"  data-saveperformance="off"  data-title="Successful Careers" data-description="">
                                                <!-- MAIN IMAGE -->
                                                <img src="{{ Voyager::image($item->img_background) }}"  alt=""  data-bgposition="top center" class="rev-slidebg" data-no-retina>
                                                <!-- LAYERS -->
                                                <div class="tp-caption tp-resizeme"
                                                     data-x="['center','center','center','center']" data-hoffset="['80','80','80','80']"
                                                     data-y="['middle','middle','middle','middle']" data-voffset="['-160','-160','-160','-160']"
                                                     data-width="none"
                                                     data-height="none"
                                                     data-transform_idle="o:1;"
                                                     data-transform_in="z:0;rX:0deg;rY:0;rZ:0;sX:1.5;sY:1.5;skX:0;skY:0;opacity:0;s:1500;e:Power3.easeInOut;"
                                                     data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;"
                                                     data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
                                                     data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
                                                     data-start="1000"
                                                     data-splitout="none"
                                                     data-responsive_offset="on"
                                                     data-elementdelay="0.05"
                                                     style="z-index: 5;">
                                                    <img src="{{ Voyager::image($item->img_icon) }}" alt="">
                                                </div>
                                                <div class="tp-caption tp-resizeme banner-caption-box"
                                                     data-x="right" data-hoffset="0"
                                                     data-y="top" data-voffset="260"
                                                     data-transform_idle="o:1;"
                                                     data-transform_in="x:[-175%];y:0px;z:0;rX:0;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0.01;s:3000;e:Power3.easeOut;"
                                                     data-transform_out="s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;"
                                                     data-mask_in="x:[100%];y:0;s:inherit;e:inherit;"
                                                     data-splitin="none"
                                                     data-splitout="none"
                                                     data-start="1000">
                                                    <div class="banner-caption-h1">{!! $item->title !!}</div>
                                                    <div class="banner-caption-p">{!! $item->sub_title !!}</div>
                                                    <a href="{{ $item->url }}" class="thm-btn contuct-us">shop now</a>
                                                </div>
                                            </li>
                                   @elseif($item->type == 3)
                                            <li data-index="rs-20" data-transition="zoomin" data-slotamount="7"  data-easein="Power4.easeInOut" data-easeout="Power4.easeInOut" data-masterspeed="2000"  data-thumb="images/home/slide-3.jpg"  data-rotate="0"  data-saveperformance="off"  data-title="Your Success" data-description="">
                                                <!-- MAIN IMAGE -->
                                                <img src="{{ Voyager::image($item->img_background) }}"  alt=""  data-bgposition="top center"  class="rev-slidebg" data-no-retina>
                                                <!-- LAYERS -->
                                                <!-- LAYER NR. 1 -->
                                                <div class="tp-caption tp-resizeme"
                                                     data-x="['center','center','center','center']" data-hoffset="['0','0','35','0']"
                                                     data-y="['middle','middle','middle','middle']" data-voffset="['-120','-120','-120','-120']"
                                                     data-width="none"
                                                     data-height="none"
                                                     data-transform_idle="o:1;"
                                                     data-transform_in="z:0;rX:0deg;rY:0;rZ:0;sX:1.5;sY:1.5;skX:0;skY:0;opacity:0;s:1500;e:Power3.easeInOut;"
                                                     data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;"
                                                     data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
                                                     data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
                                                     data-start="1000"
                                                     data-splitout="none"
                                                     data-responsive_offset="on"
                                                     data-elementdelay="0.05"
                                                     style="z-index: 5;">
                                                    <img src="{{ Voyager::image($item->img_icon) }}" alt="">
                                                </div>
                                                <!-- LAYER NR. 1 -->
                                                <div class="tp-caption tp-resizeme text-center"
                                                     data-x="center" data-hoffset="0"
                                                     data-y="center" data-voffset="-40"
                                                     data-transform_idle="o:1;"
                                                     data-transform_in="x:[-175%];y:0px;z:0;rX:0;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0.01;s:3000;e:Power3.easeOut;"
                                                     data-transform_out="s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;"
                                                     data-mask_in="x:[100%];y:0;s:inherit;e:inherit;"
                                                     data-splitin="none"
                                                     data-splitout="none"
                                                     data-start="500">
                                                    <div class="banner-caption-h2">{{ $item->title }}</div>
                                                </div>
                                                <div class="tp-caption tp-resizeme text-center"
                                                     data-x="center" data-hoffset="0"
                                                     data-y="top" data-voffset="360"
                                                     data-transform_idle="o:1;"
                                                     data-transform_in="x:[175%];y:0px;z:0;rX:0;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0.01;s:3000;e:Power3.easeOut;"
                                                     data-transform_out="s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;"
                                                     data-mask_in="x:[-100%];y:0;s:inherit;e:inherit;"
                                                     data-splitin="none"
                                                     data-splitout="none"
                                                     data-responsive_offset="on"
                                                     data-start="1400">
                                                    <div class="banner-caption-h3">{{ $item->sub_title }}</div>
                                                    <div class="banner-caption-p">{!! $item->description !!}</div>
                                                    <a href="{{ $item->url }}" class="color1-bg contuct-us">shop now</a>
                                                </div>
                                            </li>
                                    @endif
                                @endforeach
							</ul>
						</div>
					</div><!-- END REVOLUTION SLIDER -->
	        </div> <!-- End of #banner -->

			<!-- about Section ************************** -->
			<div class="about_section">
				<div class="container">
					<div class="row">
                        @foreach($promos as $item)
						    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12t">
							<div class="item wow fadeInLef" style="background-image: url({{ '/storage/' . str_replace('\\', '/', $item->img_background) }});">
								<div class="offer-sec">
                                    <div class="inner-title">{{ setting('index.discount_title') }}<div class="offer"><span>{{ $item->title_discount }}</span></div></div>
								</div>
								<div class="content">
									<h3>{{$item->title}}</h3>
                                    <p>{{ $item->description }}</p>
									<div class="link-btn"><a href="{{ $item->url }}" class="tran3s">{{ setting('index.promo_button') }}<span class="fa fa-sort-desc"></span></a></div>
								</div>
							</div>
						</div>
                        @endforeach
					</div>
				</div> <!-- End of .container -->
			</div> <!-- End of .welcome_section -->

			    <!--feature Section-->
		    <section class="featured-product">
		        <div class="container">
		            <div class="theme_title center">
		                <h3>{{ setting('index.featured_products') }}</h3>
		            </div>

		            <!--Filter-->
		            <div class="filters text-center">
		                <ul class="filter-tabs filter-btns clearfix">
                        <li class="filter active" data-role="button" data-filter="all">
                            <span class="txt">{{ 'All Products' }}</span>
                        </li>
                        @foreach($categories as $item)
                                <li class="filter" data-role="button" data-filter=".{{ $item->slug }}">
                                    <span class="txt">{{ $item->title }}</span>
                                </li>
                        @endforeach
		                </ul>
		            </div>

		            <div class="row filter-list clearfix" id="MixItUp717B05">
                        @each('layouts.components._product-card', $products, 'product')
			        </div>
		        </div>
		    </section><!-- End of section -->

			<!-- Request Quote ******************************* -->
			<section class="why_choose_us">
				<div class="theme_title_bg" style="background-image: url({{ asset('images/background/1.jpg')}});">
					<div class="theme_title center">
						<div class="container">
							<h2>{{ setting('index.why_choose_us') }}</h2>
							<p>{{ setting('index.why_choose_us_description') }}</p>
						</div>
					</div>
				</div>
				<div class="container">
					 <!-- End of .theme_title_center -->

					<div class="row">
                        @foreach($benefits as $item)
                            <!-- ______________ Item _____________ -->
                            <div class="col-md-6 col-sm-12 col-xs-12">
                                <div class="choose_us_item tran3s">
                                    <div class="icon p_color_bg border_round float_left"><span class="{{ $item->icon }}"></span></div> <!-- End of .icon -->
                                    <div class="text float_left">
                                        <h5 class="tran3s">{{ $item->title }}</h5>
                                        <p class="tran3s">{!! $item->description !!}</p>
                                    </div> <!-- End of .text -->
                                    <div class="clear_fix"></div>
                                </div> <!-- End of .choose_us_item -->
                            </div> <!-- End of .col -->
                        @endforeach
					</div>
				</div> <!-- End of .container -->
			</section> <!-- End of why chooreus -->
			<section class="news">
				<div class="container">
					<div class="theme_title center">
		                <h3>{{ setting('index.news_title') }}</h3>
		            </div>
					<div class="row">
                        @foreach($blogs as $item)
						    <div class="col-md-4 col-sm-6 col-xs-12">
							<div class="blogList_single_post clear_fix wow fadeInUp" style="visibility: visible; animation-name: fadeInUp;">
	        					<div class="img_holder">
									<img src="{{ Voyager::image($item->img) }}" alt="News" class="img-responsive">
									<div class="opacity tran3s">
										<div class="icon">
											<span><a href="#" class="border_round">+</a></span>
										</div> <!-- End of .icon -->
									</div> <!-- End of .opacity -->
								</div> <!-- End of .img_holder -->
								<div class="post">
									<ul>
										<li><a class="tran3s"><i class="fa fa-tag" aria-hidden="true"></i>{{ $item->category()->first()->title }}</a></li>
										<li><a class="tran3s"><i class="fa fa-clock-o" aria-hidden="true"></i>{{ $item->created_at }}</a></li>
										<li><a class="tran3s"><i class="fa fa-comments" aria-hidden="true"></i>{{ $item->views }}</a></li>
									</ul>
									<div class="text">
										<h4><a href="blog-details.html">{{ $item->title }}</a></h4>
										<p>{{ $item->short_description }}</p>
										<div class="link"><a href="{{ route('blog_detail', $item->slug) }}" class="tran3s">{{ setting('index.news_button') }}<span class="fa fa-sort-desc"></span></a></div>
									</div>
								</div> <!-- End of .post -->
	        				</div>
						</div>
                        @endforeach
					</div>
				</div>
			</section>

			<!-- Our Service ****************************** -->
			<div class="our_farmer">
				<div class="container">
					<div class="theme_title center">
						<h2>{{ setting('index.farmers_title') }}</h2>
					</div>

					<div class="row">
						<div class="service_slider owl-carousel owl-theme">
                            @foreach($farmers as $item)
								    <div class="item center">
									<div class="img_holder">
										<img src="{{ Voyager::image($item->img) }}" alt="images">
										<div class="overlay tran3s">
											<div class="inner-box">
												<ul>
				                        			<li><a href="{{ setting('menu.facebook') }}"><span class="fa fa-facebook"></span></a></li>
				                        			<li><a href="{{ setting('menu.twitter') }}"><span class="fa fa-twitter"></span></a></li>
				                        			<li><a href="{{ setting('menu.google') }}"><span class="fa fa-google-plus"></span></a></li>
				                        		</ul>
											</div>
				                        </div>
									</div>
									<div class="text">
										<h4>{{ $item->name }}</h4>
										<h5>{{ $item->profession }}</h5>
                                        {!! $item->description !!}
									</div>
								</div> <!-- End of .item -->
                            @endforeach
						</div> <!-- End of .service_slider -->
					</div>
				</div>
			</div> <!-- End of .our_service -->

		    <!--Testimonials Section-->
		    <section class="testimonials-section" style="background-image:url({{ asset('images/parallax/1.jpg')}});">
		        <div class="container">
		            <div class="theme_title">
						<h2>{{ setting('index.testimonials_title') }}</h2>
					</div>
		            <div class="testimonials-carousel">
		            	<!--Slide Item-->
                        @foreach($testimonials as $item)
                            <div class="slide-item">
                                <div class="inner-box">
                                    <div class="content">
                                        <div class="text-bg">
                                            <div class="quote-icon"><span class="fa fa-quote-left"></span></div>
                                            <div class="text">{!! $item->description !!}</div>
                                        </div>
                                        <div class="info clearfix">
                                            <div class="author-thumb"><img src="{{ Voyager::image($item->user->avatar) }}" alt=""></div>
                                            <div class="author">{{ $item->user->name }}</div>
                                            <div class="author-title">{{ $item->user->profession }}</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
		            </div>
		        </div>
		    </section>

			<!-- Partner Logo********************** -->
	        <div class="partners wow fadeInUp">
	        	<div class="container">
	        		<div id="partner_logo" class="owl-carousel owl-theme">
                        @foreach($partners as $item)
						    <div class="item"><img src="{{ Voyager::image($item->img) }}" alt="logo"></div>
                        @endforeach
					</div> <!-- End .partner_logo -->
				</div>
			</div>
		@include('layouts.components._subscribe')
@endsection

