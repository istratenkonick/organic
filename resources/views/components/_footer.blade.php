<footer>
    <div class="main_footer">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 footer_logo">
                    <a href="{{ route('index') }}"><img src="{{ Voyager::image(setting('footer.logo')) }}" alt="Logo"></a>
                        <p>{{ setting('footer.description_1') }}</p>
                        <p>{{ setting('footer.description_2') }}</p>
                    <a href="{{ route('products') }}" class="tran3s">{{ setting('footer.button') }}</a>
                </div> <!-- End of .footer_logo -->

                <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 footer_news">
                    <h5>recent post</h5>
                        <div class="recent-posts">
                            @foreach($blogs as $item)
                            <div class="post">
                                <div class="post-thumb">
                                    <img src="{{ Voyager::image($item->img) }}" alt="">
                                </div>
                                <h4><a href="{{ route('blog_detail', $item->slug) }}">{{ $item->title }}</a></h4>
                                <div class="post-info"><i class="fa fa-clock-o"></i>{{ $item->created_at }}</div>
                            </div>
                            @endforeach
                        </div>
                </div> <!-- End of .footer_news -->

                <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 footer_subscribe">
                    <h5>categories</h5>
                    <ul class="list catagories">
                        @foreach($categories as $item)
                        <li><a href="{{ route('products', ['category_id'=> $item->id]) }}"><i class="fa fa-angle-right"></i>{{ $item->title }}</a></li>
                        @endforeach
                    </ul>
                </div> <!-- End of .footer_subscribe -->

                <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 footer_contact">
                    <h5>{{ setting('footer.contact_title') }}</h5>
                    <ul class="list catagories">
                        <li><a href="mail:{{ setting('header.email') }}"><i class="fa fa-envelope"></i>{{ setting('header.email') }}</a></li>
                        <li><a href="tel:{{ setting('header.phone') }}"><i class="fa fa-phone"></i>{{ setting('header.phone') }}</a></li>
                        <li><a href="javascript:void(0)"><i class="fa fa-home"></i><address>{{ setting('footer.address') }}</address></a></li>
                    </ul>

                    <h5>{{ setting('footer.business_hours_title') }}</h5>
                    <div class="list Business">
                        <p>{{ setting('footer.business_hours') }}</p>
                    </div>
                </div> <!-- End of .footer_contact -->
            </div>
        </div>
    </div> <!-- End of .main_footer -->

    <div class="bottom_footer clear_fix">
        <div class="container">
            <h6 class="pull-left">{{ setting('footer.copyright') }}<a href="{{ setting('footer.copyright_url') }}" target="_blank"> {{ setting('footer.copyright_title') }}</a></h6>
            <ul class="social_icon pull-right">
                <li><a href="{{ setting('footer.visa') }}" class="tran3s"><i class="fa fa-cc-visa" aria-hidden="true"></i></a></li>
                <li><a href="{{ setting('footer.mastercard') }}" class="tran3s"><i class="fa fa-cc-mastercard" aria-hidden="true"></i></a></li>
                <li><a href="{{ setting('footer.paypal') }}" class="tran3s"><i class="fa fa-paypal" aria-hidden="true"></i></a></li>
            </ul>
        </div>
    </div>
</footer>
