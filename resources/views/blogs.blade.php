@extends('layouts.app')
    @section('content')
			<section class="breadcrumb-area" style="background-image:url({{ asset('images/background/2.jpg')}})">
			    <div class="container">
			        <div class="row">
			            <div class="col-md-12">
			                <div class="breadcrumbs text-center">
			                    <h1>{{ setting('news.breadcrumb_title') }}</h1>
			                    <h4>{{ setting('news.breadcrumb_subtitle') }}</h4>
			                </div>
			            </div>
			        </div>
			    </div>
				<div class="breadcrumb-bottom-area">
				    <div class="container">
				        <div class="row">
				            <div class="col-lg-8 col-md-5 col-sm-5">
				                <ul>
				                    <li><a href="{{ route('index') }}">Home</a></li>
				                    <li><i class="fa fa-angle-right"></i></li>
				                    <li>Blogs</li>
				                </ul>
				            </div>
				            <div class="col-lg-4 col-md-7 col-sm-7">
				                <p>{{ setting('news.breadcrumb_description') }}</p>
				            </div>
				        </div>
				    </div>
				</div>
			</section>

			<section class="news single_news_page style-2">
				<div class="container">
					<div class="row">
                        @foreach($blogs as $item)
						    <div class="col-md-4 col-sm-6 col-xs-12">
							<div class="blogList_single_post clear_fix wow fadeInUp" style="visibility: visible; animation-name: fadeInUp;">
	        					<div class="img_holder">
									    <img src="{{ Voyager::image($item->img) }}" alt="News" class="img-responsive">
									<div class="opacity tran3s">
										<div class="icon">
											<span><a href="{{ route('blog_detail', $item->slug) }}" class="border_round">+</a></span>
										</div> <!-- End of .icon -->
									</div> <!-- End of .opacity -->
								</div> <!-- End of .img_holder -->
								<div class="post">
									<ul>
										<li><a class="tran3s"><i class="fa fa-tag" aria-hidden="true"></i>{{ $item->category()->first()->title }}</a></li>
										<li><a class="tran3s"><i class="fa fa-clock-o" aria-hidden="true"></i>{{ $item->created_at }}</a></li>
										<li><a class="tran3s"><i class="fa fa-comments" aria-hidden="true"></i>{{ $item->views }}</a></li>
									</ul>
									<div class="text">
										<h4><a href="{{ route('blog_detail', $item->slug) }}">{{ $item->title }}</a></h4>
										<p>{{ $item->shot_description }}</p>
										<div class="link"><a href="{{ route('blog_detail', $item->slug) }}" class="tran3s">{{ setting('news.button') }}<span class="fa fa-sort-desc"></span></a></div>
									</div>
								</div> <!-- End of .post -->
	        				</div>
						</div>
                        @endforeach
					</div>
				</div>
			</section>
    @endsection
