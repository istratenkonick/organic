@extends('layouts.app')
@section('content')
        <section class="breadcrumb-area" style="background-image:url({{ asset('images/background/2.jpg')}});">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="breadcrumbs text-center">
                            <h1>{{ setting('store-detail.breadcrumb_title') }}</h1>
                            <h4>{{ setting('store-detail.breadcrumb_subtitle') }}</h4>
                        </div>
                    </div>
                </div>
            </div>
            <div class="breadcrumb-bottom-area">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-8 col-md-5 col-sm-5">
                            <ul>
                                <li><a href="{{ route('index') }}">Home</a></li>
                                <li><a href="#"><i class="fa fa-angle-right"></i></a></li>
                                <li><a href="{{ route('products') }}">Products</a></li>
                                <li><a href="#"><i class="fa fa-angle-right"></i></a></li>
                                <li>Single product</li>
                            </ul>
                        </div>
                        <div class="col-lg-4 col-md-7 col-sm-7">
                            <p>{{ setting('store-detail.breadcrumb_description') }}</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!-- Single Shop page content ________________ -->
        <div class="shop_single_page">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 product_details">
                        <div class="wrapper">
                            <div class="product_top_section clear_fix">
                                <div class="img_holder float_left">
                                    <img src="{{ Voyager::image($product->img) }}" alt="img" class="img-responsive">
                                </div> <!-- End of .img_holder -->
                                <div class="item_description float_left">
                                    <h4>{{ $product->title }}</h4>
                                    <ul>
                                        @for($i = 0; $i < $product->star; $i++)
                                            <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                        @endfor
                                        <li>({{ $product->comments()->count() }} Customers Review)</li>
                                    </ul>
                                    <span class="item_price">{{ $product->price }}</span>
                                    {!! $product->description !!}
                                </div> <!-- End of .item_description -->
                            </div> <!-- End of .product_top_section -->

                            <!-- __________________ Product review ___________________ -->
                            <div class="product-review-tab">
                                <ul class="nav nav-pills">
                                    <li><a data-toggle="pill" href="#tab1">Description</a></li>
                                    <li class="active"><a data-toggle="pill" href="#tab2">Reviews({{ $product->comments()->count() }})</a></li>
                                </ul>
                                 <div class="tab-content">
                                    <div id="tab1" class="tab-pane fade">
                                        {!! $product->description !!}
                                      <p style="margin-top:10px">{!! $product->description !!}</p>
                                    </div> <!-- End of #tab1 -->

                                    <div id="tab2" class="tab-pane fade in active">
                                      <!-- Single Review -->
                                        @foreach($product->comments()->get() as $item)
                                             <div class="item_review_content clear_fix">
                                        <div class="img_holder float_left">
                                        </div> <!-- End of .img_holder -->

                                        <div class="text float_left">
                                            <div class="sec_up clear_fix">
                                                <h6 class="float_left">{{ $item->name }}</h6>
                                                <div class="float_right">
                                                    <span class="p_color">{{ $item->created_at }}</span>
                                                </div>
                                            </div> <!-- End of .sec_up -->
                                            <p>{{ $item->review }}</p>
                                            <div class="up_down_nav">
                                                <a href="#"><i class="fa fa-angle-up" aria-hidden="true"></i></a>
                                                <a href="#"><i class="fa fa-angle-down" aria-hidden="true"></i></a>
                                            </div> <!-- End of .up_down_nav -->

                                        </div> <!-- End of .text -->
                                      </div>
                                        @endforeach
                                      <div class="add_your_review">
                                            <div class="theme_inner_title">
                                                <h4>Add Your Review</h4>
                                            <form action="{{ route('product_review') }}" method="POST">
                                                @csrf
                                                <div class="row">
                                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                        <input name="name" type="text" placeholder="Name*">
                                                    </div>
                                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                        <input name="email" type="email" placeholder="Email*">
                                                    </div>
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <textarea name="review" placeholder="Your Review..."></textarea>
                                                    </div>
                                                    @if($errors->any())
                                                        @foreach($errors->all() as $error)
                                                            <p style="color: red; padding: 10px 15px; font-size: 30px">{{ $error }}</p>
                                                        @endforeach
                                                    @endif
                                                    @if(session()->has('msg'))
                                                    <div><p style="color: red; padding: 10px 15px; font-size: 30px">Success!!!</p></div>
                                                    @endif
                                                    <input name="product_id" type="product_id" value="{{ $product->id }}" style="display: none">
                                                </div>
                                                <button class="color1_bg tran3s">Add A Review</button>
                                            </form>
                                      </div> <!-- End of .add_your_review -->
                                    </div> <!-- End of #tab2 -->
                                 </div> <!-- End of .tab-content -->
                            </div> <!-- End of .product-review-tab -->

                            <div class="related_product">
                                <div class="theme_title">
                                    <h3>Related Products</h3>
                                </div>

                                <!-- Shop Page Content************************ -->
                                <div class="shop_page featured-product">
                                    <div class="row">

                                        <!--Default Item-->
                                        @foreach($relatives as $item)
                                        <div class="col-md-4 col-sm-6 col-xs-12 default-item" style="display: inline-block;">
                                            <div class="inner-box">
                                                <div class="single-item center">
                                                    <figure class="image-box"><img src="{{ Voyager::image($item->img) }}" alt="">
                                                        @if($item->is_new)<div class="product-model new">New</div>@endif
                                                        @if($item->is_hot)<div class="product-model hot">Hot</div>@endif
                                                    </figure>
                                                    <div class="content">
                                                        <h3><a href="{{ route('product_detail', $item->slug) }}">{{ $item->title }}</a></h3>
                                                        <div class="rating">
                                                            @for($i = 0; $i < $item->star; $i++)
                                                                <span class="fa fa-star"></span>
                                                            @endfor
                                                        </div>
                                                        <div class="price">${{ $item->price }}<span class="prev-rate">${{ $item->old_price }}</span></div>
                                                    </div>
                                                    <div class="overlay-box">
                                                        <div class="inner">
                                                            <div class="top-content">
                                                                <ul>
                                                                    <li><a href="#"><span class="fa fa-eye"></span></a></li>
                                                                    <li class="tultip-op"><span class="tultip"><i class="fa fa-sort-desc"></i>ADD TO CART</span><a href="#"><span class="icon-icon-32846"></span></a>
                                                                    </li>
                                                                    <li><a href="#"><span class="fa fa-heart-o"></span></a></li>
                                                                </ul>
                                                            </div>
                                                            <div class="bottom-content">
                                                                <h4><a href="{{ route('product_detail', $item->slug) }}">It Contains:</a></h4>
                                                                <p>{{ $item->short_description }}</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        @endforeach
                                    </div> <!-- End of .row -->
                                </div> <!-- End of .shop_page -->
                            </div> <!-- End of .related_product -->
                        </div> <!-- End of .wrapper -->
                    </div> <!-- End of .col -->
                </div> <!-- End of .row -->
            </div> <!-- End of .container -->
        </div> <!-- End of .shop_single_page -->

@endsection
