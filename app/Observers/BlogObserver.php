<?php

namespace App\Observers;

use App\Models\Blog;
use App\Models\Subscriber;
use App\Models\User;
use App\Notifications\BlogNotification;
use App\Notifications\ProductNotification;
use Illuminate\Support\Facades\Notification;

class BlogObserver
{
    /**
     * Handle the Blog "created" event.
     *
     * @param  \App\Models\Blog  $blog
     * @return void
     */
    public function created(Blog $blog)
    {
        if($blog->should_be_send == 1){
            $subscribers = Subscriber::all();
            foreach($subscribers as $subscriber){
                $subscriber->notify(new BlogNotification($blog));
            }
        }


    }

    /**
     * Handle the Blog "updated" event.
     *
     * @param  \App\Models\Blog  $blog
     * @return void
     */
    public function updated(Blog $blog)
    {
        //
    }

    /**
     * Handle the Blog "deleted" event.
     *
     * @param  \App\Models\Blog  $blog
     * @return void
     */
    public function deleted(Blog $blog)
    {
        //
    }

    /**
     * Handle the Blog "restored" event.
     *
     * @param  \App\Models\Blog  $blog
     * @return void
     */
    public function restored(Blog $blog)
    {
        //
    }

    /**
     * Handle the Blog "force deleted" event.
     *
     * @param  \App\Models\Blog  $blog
     * @return void
     */
    public function forceDeleted(Blog $blog)
    {
        //
    }
}
