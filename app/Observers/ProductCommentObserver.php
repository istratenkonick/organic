<?php

namespace App\Observers;

use App\Models\ProductComment;
use App\Models\User;
use App\Notifications\NotifyAdmin;
use Illuminate\Support\Facades\Notification;

class ProductCommentObserver
{
    /**
     * Handle the ProductComment "created" event.
     *
     * @param  \App\Models\ProductComment  $product_comment
     * @return void
     */
    public function created(ProductComment $product_comment)
    {
       $admins = User::where('role_id', '=', 1)->get();
            foreach($admins as $admin){
                $admin->notify(new NotifyAdmin($product_comment));
            }

    }

    /**
     * Handle the ProductComment "updated" event.
     *
     * @param  \App\Models\ProductComment  $product_comment
     * @return void
     */
    public function updated(ProductComment $product_comment)
    {
        //
    }

    /**
     * Handle the ProductComment "deleted" event.
     *
     * @param  \App\Models\ProductComment  $productComment
     * @return void
     */
    public function deleted(ProductComment $productComment)
    {
        //
    }

    /**
     * Handle the ProductComment "restored" event.
     *
     * @param  \App\Models\ProductComment  $productComment
     * @return void
     */
    public function restored(ProductComment $productComment)
    {
        //
    }

    /**
     * Handle the ProductComment "force deleted" event.
     *
     * @param  \App\Models\ProductComment  $productComment
     * @return void
     */
    public function forceDeleted(ProductComment $productComment)
    {
        //
    }
}
