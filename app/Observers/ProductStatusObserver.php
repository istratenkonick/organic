<?php

namespace App\Observers;

use App\Models\Product;
use App\Models\User;
use App\Notifications\NotifyAdmin;
use App\Notifications\StatusNotification;

class ProductStatusObserver
{
    /**
     * Handle the Product "created" event.
     *
     * @param  \App\Models\Product  $product_status
     * @return void
     */
    public function created(Product $product_status)
    {

    }

    /**
     * Handle the Product "updated" event.
     *
     * @param  \App\Models\Product  $product
     * @return void
     */
    public function updated(Product $product)
    {
        if($product->isDirty('status')){
            $admins = User::where('role_id', '=', 1)->get();
            foreach($admins as $admin){
                $admin->notify(new StatusNotification($product));
            }
        }
    }

    /**
     * Handle the Product "deleted" event.
     *
     * @param  \App\Models\Product  $product
     * @return void
     */
    public function deleted(Product $product)
    {
        //
    }

    /**
     * Handle the Product "restored" event.
     *
     * @param  \App\Models\Product  $product
     * @return void
     */
    public function restored(Product $product)
    {
        //
    }

    /**
     * Handle the Product "force deleted" event.
     *
     * @param  \App\Models\Product  $product
     * @return void
     */
    public function forceDeleted(Product $product)
    {
        //
    }
}
