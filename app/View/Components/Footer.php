<?php

namespace App\View\Components;

use App\Models\Blog;
use App\Models\ProductCategory;
use Illuminate\View\Component;
use TCG\Voyager\Models\Category;

class Footer extends Component
{
    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        $blogs = Blog::inRandomOrder()->take(3)->get();

        $categories = ProductCategory::where('status', 1)
            ->select('id', 'title', 'slug')
            ->get();


        return view('components._footer', compact('blogs', 'categories'));
    }
}
