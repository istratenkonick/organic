<?php

namespace App\Providers;

use App\Models\Blog;
use App\Models\Product;
use App\Models\ProductComment;
use App\Observers\BlogObserver;
use App\Observers\ProductCommentObserver;
use App\Observers\ProductObserver;
use App\Observers\ProductStatusObserver;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        Product::observe(ProductObserver::class);
        Blog::observe(BlogObserver::class);
        ProductComment::observe(ProductCommentObserver::class);
        Product::observe(ProductStatusObserver::class);
    }
}
