<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CommentRequest extends FormRequest
{
    public function rules()
    {
        return [
            'name' => ['min:3', 'max:25', 'required'],
            'email' =>['min:3', 'max:50', 'required', 'email'],
            'comment' => ['min:3', 'max:2000', 'required'],
            'website' => ['min:3', 'max:50', 'required'],
        ];
    }
}
