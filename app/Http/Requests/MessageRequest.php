<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MessageRequest extends FormRequest
{

    public function rules()
    {
        return [
            'name' => ['min:3', 'max:25', 'required'],
            'email' =>['min:3', 'max:50', 'required', 'email'],
            'phone' => ['required', 'regex:/^([0-9\s\-\+\(\)]*)$/'],
        ];
    }
}
