<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ReviewRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'min:3|max:25|required',
            'email' => 'min:3|max:50|required|email',
            'review' => 'min:3|max:2000|required',
            'product_id' => 'required|exists:products,id',
        ];
    }
}
