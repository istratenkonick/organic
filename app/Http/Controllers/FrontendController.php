<?php


namespace App\Http\Controllers;


use App\Http\Requests\CommentRequest;
use App\Http\Requests\MessageRequest;
use App\Http\Requests\ReviewRequest;
use App\Http\Requests\SubscriberRequest;
use App\Models\Award;
use App\Models\BlogComment;
use App\Models\Delivery;
use App\Models\Faq;
use App\Models\Message;
use App\Models\Promo;
use App\Models\Banner;
use App\Models\Benefit;
use App\Models\Blog;
use App\Models\Farmer;
use App\Models\Page;
use App\Models\Partner;
use App\Models\Product;
use App\Models\ProductCategory;
use App\Models\ProductComment;
use App\Models\Subscriber;
use App\Models\Testimonial;
use App\Services\EmployerServices;
use ChrisKonnertz\OpenGraph\OpenGraph;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Spatie\SchemaOrg\Graph;

class FrontendController extends Controller
{
    public function index()
    {
        $page = Page::where('slug', '/')
            ->where('status', 1)
            ->firstOrFail();
        $meta_title = $page->meta_title;
        $meta_description = $page->meta_description;

        $banners = Banner::where('status', 1)
            ->take(3)
            ->get();

        $promos = Promo::where('status', 1)
            ->select('id', 'img_background', 'title_discount', 'title', 'description', 'url', 'status')
            ->take(2)
            ->get();

        $products = Product::where('status', 1)
            ->select('id', 'img', 'title', 'price', 'old_price',
                'short_description', 'is_hot', 'is_new', 'slug', 'star', 'category_id')
            ->take(8)
            ->get();

        $benefits = Benefit::where('status', 1)
            ->select('id', 'icon', 'title', 'description', 'status')
            ->take(4)
            ->get();

        $blogs = Blog::where('status', 1)
            ->select('id', 'views', 'title', 'short_description',
                'description', 'img', 'author_comment', 'user_id',
                'category_id', 'created_at', 'slug')
            ->take(3)
            ->get();

        $farmers = Farmer::where('status', 1)
            ->select('id', 'img', 'name', 'profession', 'description')
            ->take(4)
            ->get();

        $testimonials = Testimonial::where('status', 1)
            ->select('id', 'user_id', 'description')
            ->take(6)
            ->get();

        $partners = Partner::where('status', 1)
            ->select('id', 'img')
            ->take(4)
            ->get();

        $categories = ProductCategory::where('status', 1)
            ->select('id', 'title', 'slug')
            ->get();

        $og = (new \ChrisKonnertz\OpenGraph\OpenGraph)->title($page->meta_title)
            ->description($page->meta_description)
            ->url(route('index'));

        $farmers = app(EmployerServices::class); // DI contrainer

        $graph = new Graph();
        $graph->organization()
            ->brand('Organic Store')
            ->address('Moscova 7')
            ->email('contact@organic.org')
            ->founder('Nick Istratenko')
            ->employee($farmers->employee());

      $graph = $graph->toScript();

//        /**
//         * @var Collection $products
//         */
//        $filtered = '';
//        $mapped = '';
//        dd();
        return view('index', compact('banners', 'promos', 'products',
            'benefits', 'blogs', 'farmers',
            'testimonials', 'partners', 'page',
            'meta_description', 'meta_title', 'categories', 'og', 'graph'));
    }

    /**
     * @throws \Exception
     */
    public function productDetail($slug)
    {
        $product = Product::where('slug', $slug)
            ->where('status', 1)
            ->firstOrFail();

        $relatives = Product::where('status', 1)
            ->where('is_related', 1)
            ->select('id', 'img', 'title', 'price',
                'old_price', 'short_description', 'is_hot',
                'is_new', 'slug', 'star')
            ->get();

        $og = (new \ChrisKonnertz\OpenGraph\OpenGraph)->title($product->title)
            ->type('article')
            ->image($product->img)
            ->description($product->description)
            ->url($product->slug);

        $meta_title = $product->title;
        $meta_description = $product->description;

        $graph = new Graph();
        $graph->product()
            ->name($product->title)
            ->description($product->short_description)
            ->image($product->img)
            ->category($product->category_id)
            ->offers([
                'price' => $product->price,
                'url' => \request()->url(),
                'priceCurrency' => 'USD',
            ]);

        $graph = $graph->toScript();

        return view('shop-single', compact('product',
            'relatives', 'og', 'meta_description', 'meta_title', 'graph'));
    }

    public function productReview(ReviewRequest $request)
    {
        ProductComment::create($request->all());

        return redirect()->back()->with('msg' , 'Success');
    }

    public function subscribe(SubscriberRequest $request)
    {
        Subscriber::create($request->all());

        return redirect()->back()->with('msg', 'Успешно подписался!');
    }

    public function products(Request $request)
    {
        $page = Page::where('slug', 'products')
            ->where('status', 1)
            ->firstorFail();
        $meta_title = $page->meta_title;
        $meta_description = $page->meta_description;

        $products = Product::query()->where('status', 1)
            ->select('id', 'img', 'title', 'price',
                'old_price', 'short_description', 'is_hot',
                'is_new', 'slug', 'star', 'category_id');

        if ($request->has('category_id')){
            $products->whereHas('category', function ($query) use ($request){
                $query->where('id', $request->get('category_id'));
            });
        }

        if ($request->has('s')) {
            $products->where('title', 'LIKE', '%' . $request->get('s') . '%');
        }

        if ($request->has('price_min', 'price_max')) {
            $products->whereBetween('price', [
                $request->get('price_min'),
                $request->get('price_max')
            ]);
        }


        $products = $products->get();

        $categories = ProductCategory::where('status', 1)
            ->select('id', 'title')
            ->withCount('products')
            ->get();

        $popular = Product::where('status', 1)
            ->where('is_popular', 1)
            ->select('id', 'is_popular', 'title', 'price', 'img', 'star', 'slug')
            ->get();


        $og = (new \ChrisKonnertz\OpenGraph\OpenGraph)->title($page->meta_title)
            ->type('article')
            ->description($page->meta_description)
            ->url(route('products'));


        return view('shop', compact('products', 'categories', 'popular',
            'og', 'page', 'meta_title', 'meta_description'));
    }

    public function aboutUs()
    {
        $page = Page::where('slug', 'about-us')
            ->where('status', 1)
            ->firstorFail();
        $meta_title = $page->meta_title;
        $meta_description = $page->meta_description;

        $benefits = Benefit::where('status', 1)
            ->select('id', 'icon', 'title', 'description', 'status')
            ->take(3)
            ->get();

        $deliveries = Delivery::where('status', 1)
            ->select('id', 'order', 'title', 'description', 'icon')
            ->take('4')
            ->get();

        $categories = ProductCategory::with('products')->where('status', 1)
            ->select('id', 'title', 'slug')
            ->get();

        $awards = Award::where('status', 1)
            ->select('id', 'img')
            ->get();

        $og = (new \ChrisKonnertz\OpenGraph\OpenGraph)->title($page->meta_title)
            ->type('article')
            ->description($page->meta_description)
            ->url(route('about_us'));

        return view('about-us', compact('benefits',
            'deliveries', 'categories', 'awards', 'page', 'og', 'meta_title', 'meta_description'));
    }

    public function message(MessageRequest $request)
    {
        Message::create($request->all());

        return redirect()->back()->with('msg', 'Успешно подписался!');
    }

    public function blogs()
    {
        $page = Page::where('slug', 'news')
            ->where('status', 1)
            ->firstorFail();
        $meta_title = $page->meta_title;
        $meta_description = $page->meta_description;

        $blogs = Blog::where('status', 1)
            ->take(9)
            ->get();

        $og = (new \ChrisKonnertz\OpenGraph\OpenGraph)->title($page->meta_title)
            ->type('article')
            ->description($page->meta_description)
            ->url(route('blogs'));

        return view('blogs', compact('blogs', 'page', 'og', 'meta_title', 'meta_description'));
    }

    public function blogDetail($slug)
    {
        $blog = Blog::where('slug', $slug)
            ->where('status', 1)
            ->firstOrFail();

        $og = (new \ChrisKonnertz\OpenGraph\OpenGraph)->title($blog->title)
            ->type('article')
            ->image($blog->img)
            ->description($blog->description)
            ->url($blog->slug);

        $meta_title = $blog->title;
        $meta_description = $blog->description;

        $blog->views++;
        $blog->save();

        $graph = new Graph();
        $graph->blog()
            ->name($blog->title)
            ->description($blog->short_description)
            ->image($blog->img)
            ->dateCreated($blog->created_at)
            ->author($blog->user_id)
            ->comment($blog->author_comment);

        $graph = $graph->toScript();

        return view('blog-details', compact('blog', 'og', 'meta_description', 'meta_title', 'graph'));
    }

    public function blogComment(CommentRequest $request)
    {
        BlogComment::create($request->all());

        return redirect()->back()->with('msg', 'Успешно подписался!');
    }

    public function faq()
    {
        $page = Page::where('slug', 'faq')
            ->where('status', 1)
            ->firstorFail();
        $meta_title = $page->meta_title;
        $meta_description = $page->meta_description;

        $faq = Faq::where('status', 1)
            ->get();

        $og = (new \ChrisKonnertz\OpenGraph\OpenGraph)->title($page->meta_title)
            ->type('article')
            ->description($page->meta_description)
            ->url(route('faq'));

        return view('faq', compact('faq', 'og', 'page', 'meta_title', 'meta_description'));
    }

    public function contact()
    {
        $page = Page::where('slug', 'contact-us')
            ->where('status', 1)
            ->firstorFail();

        $meta_title = $page->meta_title;
        $meta_description = $page->meta_description;

        $og = (new \ChrisKonnertz\OpenGraph\OpenGraph)->title($page->meta_title)
            ->type('article')
            ->description($page->meta_description)
            ->url(route('contact_us'));

        return view('contact', compact('og', 'page', 'meta_title', 'meta_description'));
    }

    public function search(Request $request)
    { // -- Validation
        if ($request->search !== '') {
            $products = Product::query()
                ->where('title', 'LIKE', '%' . $request->search . '%')
                ->orwhere('slug', 'LIKE', '%' . $request->search . '%')
                ->take(3)
                ->get();

            $blogs = Blog::query()
                ->where('title', 'LIKE', '%' . $request->search . '%')
                ->orWhere('slug', 'LIKE', '%' . $request->search . '%')
                ->take(3)
                ->get();

            return view('layouts.components._search_box', compact('products', 'blogs'));
        }
        return '';
    }

}
