<?php


namespace App\Services;


use App\Models\Farmer;

class EmployerServices
{
    public function employee() {
        $farmers = Farmer::all();
        $arr = [];
        foreach($farmers as $farmer){
           $arr[]['givenName'] = $farmer->name;
        }
         return $arr;
    }
}
