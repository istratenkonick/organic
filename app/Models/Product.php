<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;


class Product extends Model
{
    use HasFactory;
    use HasSlug;

    protected $guarded = [];

    public function category()
    {
        return $this->belongsTo(ProductCategory::class);
    }

    public function comments()
    {
        return $this->hasMany(ProductComment::class);
    }

    public function getSlugOptions(): SlugOptions
    {
        return SlugOptions::create()
        ->generateSlugsFrom('title')
        ->saveSlugsTo('slug');
    }

    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }
}
