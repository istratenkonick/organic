<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBlogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blogs', function (Blueprint $table) {
            $table->id();
            $table->integer('category_id');
            $table->integer('views');
            $table->string('title', 255);
            $table->string('short_description', 5000);
            $table->string('description', 5000);
            $table->string('slug', 255)->unique();
            $table->string('img', 255);
            $table->integer('user_id');
            $table->string('author_comment', 255);
            $table->boolean('status')->default(0)->nullable();
            $table->boolean('should_be_send')->default(0)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('blogs');
    }
}
