<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string('img', 255);
            $table->string('title', 255);
            $table->integer('price');
            $table->integer('old_price');
            $table->string('short_description', 255);
            $table->string('description', 5000);
            $table->tinyInteger('is_hot')->default(0)->nullable();
            $table->tinyInteger('is_new')->default(0)->nullable();
            $table->tinyInteger('is_related')->default(0)->nullable();
            $table->integer('is_popular');
            $table->string('slug', 255)->unique();
            $table->integer('category_id');
            $table->string('status', 255);
            $table->integer('star')->default(5);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
