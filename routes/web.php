<?php

use App\Http\Controllers\FrontendController;
use App\Http\Controllers\PaymentController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [FrontendController::class, 'index'])
    ->name('index');

Route::post('/subscribe', [FrontendController::class, 'subscribe'])
    ->name('subscribe');

Route::prefix('products')->group(function () {

    Route::get('/', [FrontendController::class, 'products'])
        ->name('products');

    Route::get('/{slug}', [FrontendController::class, 'productDetail'])
        ->name('product_detail');

    Route::post('/review' , [FrontendController::class, 'productReview'])
        ->name('product_review');
});

Route::get('/about-us', [FrontendController::class, 'aboutUs'])
    ->name('about_us');

Route::post('/message', [FrontendController::class, 'message'])
    ->name('message');

Route::prefix('blogs')->group(function () {

    Route::get('/', [FrontendController::class, 'blogs'])
        ->name('blogs');

    Route::get('/{slug}', [FrontendController::class, 'blogDetail'])
        ->name('blog_detail');

    Route::post('/comment', [FrontendController::class, 'blogComment'])
        ->name('blog_comment');
});

Route::get('/faq', [FrontendController::class, 'faq'])
    ->name('faq');

Route::get('/contact', [FrontendController::class, 'contact'])
    ->name('contact_us');

Route::get('/search', [FrontendController::class, 'search'])
    ->name('search');

Route::post('/paypal', [PaymentController::class, 'payWithPaypal'])
    ->name('paypal');

Route::get('/status', [PaymentController::class, 'getPaymentStatus'])
    ->name('status');

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});

//'/products' - GET
//'/products/{slug}' - GET
//'/products/create' - GET
//'/products' - POST
//'/products/1/edit' - GET
//'/products/1' - POST/PUT
//'/products/1' - DELETE
